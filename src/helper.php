<?php

/**
 * WeEngine Document System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

use Symfony\Component\VarDumper\VarDumper;

if (!function_exists('dd')) {
	function dd(...$args)
	{
		foreach ($args as $arg) {
			VarDumper::dump($arg);
		}
	}
}

if (!function_exists('jd')) {
	function jd(...$vars)
	{
		$option = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE;
		$traces = debug_backtrace();

		$args = [
			"{$traces[0]['file']}:{$traces[0]['line']}",
		];
		foreach ($vars as $var) {
			if ($var instanceof \Illuminate\Database\Eloquent\Model) {
				$var = $var->getAttributes();
			} elseif ($var instanceof \Illuminate\Database\Eloquent\Collection) {
				$var = $var->map(function (\Illuminate\Database\Eloquent\Model $model) {
					return $model->getAttributes();
				})->toArray();
			}
			$args[] = json_encode($var, $option);
		}
		dd(...$args);
	}
}

if (!function_exists('ll')) {
	function ll(...$vars)
	{
		$option = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE;
		$traces = debug_backtrace();

		$args = [
			"{$traces[0]['file']}:{$traces[0]['line']}",
		];
		foreach ($vars as $var) {
			if ($var instanceof \Illuminate\Database\Eloquent\Model ||
				$var instanceof \Illuminate\Database\Eloquent\Collection) {
				$args[] = $var->toJson($option);
			} else {
				$args[] = json_encode($var, $option);
			}
		}

		foreach ($args as $x) {
			dump($x);
//			(new Illuminate\Support\Debug\Dumper)->dump($x);
		}
	}
}

if (!function_exists('ul')) {
	function ul(...$vars)
	{
		$option = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE;
		$traces = debug_backtrace();

		$args = [
			"{$traces[0]['file']}:{$traces[0]['line']}",
		];
		foreach ($vars as $var) {
			if ($var instanceof \Illuminate\Database\Eloquent\Model ||
				$var instanceof \Illuminate\Database\Eloquent\Collection) {
				$args[] = $var->toJson($option);
			} else {
				$args[] = json_encode($var, $option);
			}
		}

		foreach ($args as $x) {
			dump($x);
		}
	}
}

if (!function_exists('define_unit_test')) {
	function define_unit_test()
	{
		if (!defined('IS_UNIT_TEST')) {
			define('IS_UNIT_TEST', true);
		}
	}
}

if (!function_exists('is_unit_test')) {
	function is_unit_test()
	{
		return defined('IS_UNIT_TEST');
	}
}
