# ViewModel

#### 介绍

> 提供只读数据,强类型访问.

> Laravel Model 数据访问层, ViewModel 业务展示层, 多项目 Model 共享.

#### 软件架构
 

#### 安装教程

#### 使用说明

1. 创建模型对象继承 ViewModel.
2. ViewModel 用法与 Model 类似.
3. 要使用强类型提示,定义好模型结构,完善类型注释
4. 

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
