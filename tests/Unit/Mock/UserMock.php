<?php

namespace Tests\Unit\Mock;

use Tests\Model\Order\User;

class UserMock extends User
{
	/**
	 * @param $id
	 * @return \Illuminate\Database\Eloquent\Model|null|static
	 */
	public static function findById($id)
	{
		return static::query()->find($id);
	}
}