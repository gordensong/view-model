<?php


namespace Tests\Unit\Mock;



use Tests\Model\Order\Product;

class ProductMock extends Product
{
	/**
	 * @param $id
	 * @return \Illuminate\Database\Eloquent\Model|null|static
	 */
	public static function findById($id)
	{
		return static::query()->find($id);
	}
}