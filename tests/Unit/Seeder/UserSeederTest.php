<?php

namespace Tests\Unit\Seeder;

use Illuminate\Support\Facades\DB;
use Tests\Database\Migration\Order\UserTable;
use Tests\Database\Seeder\Order\UserSeeder;
use Tests\TestCase;

class UserSeederTest extends TestCase
{
	public function test_run()
	{
		(new UserTable())->down()->up();

		$seeder = new UserSeeder();
		$seeder->run();

		$userCount = DB::table('user')->count();
		self::assertEquals(3, $userCount);
	}
}