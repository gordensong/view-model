<?php

namespace Tests\Unit\Migration;

use Illuminate\Support\Facades\Schema;
use Tests\Database\Migration\Order\UserTable;
use Tests\TestCase;

class UserTableTest extends TestCase
{
	public function test_create()
	{
		$table = new UserTable();
		$table->down()->up();

		$hasUser = Schema::connection('default')->hasTable('user');
		self::assertTrue($hasUser);
	}
}