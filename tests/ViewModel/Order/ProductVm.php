<?php


namespace Tests\ViewModel\Order;


use Sjj\Eloquent\ViewModel;

class ProductVm extends ViewModel
{
	protected $hidden = ['created_at', 'updated_at'];
}