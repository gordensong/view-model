<?php

namespace Tests\ViewModel\Order;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Sjj\Eloquent\ViewModel;
use Tests\Model\Order\Order;
use Tests\Model\Order\OrderItem;

class OrderVm extends ViewModel
{
	protected $hidden = ['created_at', 'updated_at'];

	/**
	 * @var UserVm
	 */
	public $user;

	/**
	 * @var OrderItemVm[]|Collection
	 */
	public $items;

	/**
	 * Order constructor.
	 * @param Model|Order $model
	 */
	public function __construct(Model $model)
	{
		parent::__construct($model);

		$this->belongsTo('user', new UserVm($model->user));

		$this->hasMany('items', $model->items->map(function (OrderItem $orderItem) {
			return new OrderItemVm($orderItem);
		}));
	}
}