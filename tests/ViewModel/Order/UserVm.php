<?php


namespace Tests\ViewModel\Order;


use Sjj\Eloquent\ViewModel;

/**
 * Class User
 * @package Tests\ViewModel\Order
 */
class UserVm extends ViewModel
{
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['join_time'];

	protected $dateFormat = 'Y-m-d';
}
