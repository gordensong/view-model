<?php


namespace Tests\ViewModel\Order;


use Illuminate\Database\Eloquent\Model;
use Sjj\Eloquent\ViewModel;
use Tests\Model\Order\OrderItem;
use Tests\Model\Order\Product;

class OrderItemVm extends ViewModel
{
	/**
	 * @var ProductVm
	 */
	public $product;

	protected $casts = [
		'amount' => 'int',
		'order_id' => 'int',
	];

	/**
	 * OrderItem constructor.
	 * @param Model|OrderItem $model
	 */
	public function __construct(Model $model)
	{
		parent::__construct($model);

		$this->belongsTo('product', new ProductVm($model->product ?? new Product()));
	}
}