<?php

/**
 * WeEngine Sdk Core System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace Tests\Database\Migration;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Builder;
use Illuminate\Support\Facades\Schema;

abstract class Table extends Migration
{
	protected $table = null;

	protected $prefix = 'ims_';

	private $scheme = 'default';

	/**
	 * @return Builder
	 */
	protected function scheme()
	{
		return Schema::connection($this->scheme);
	}

	/**
	 * @param $callback
	 */
	public function create($callback)
	{
		$this->scheme()->create($this->table, $callback);
	}

	/**
	 * @return $this
	 */
	public function down()
	{
		if (empty($this->table)) {
			throw new \InvalidArgumentException('未设置表名');
		}

		$this->scheme()->dropIfExists($this->table);

		return $this;
	}

	abstract public function up();
}
