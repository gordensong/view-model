<?php

namespace Tests\Database\Migration\Order;

use Illuminate\Database\Schema\Blueprint;
use Tests\Database\Migration\Table;

class UserTable extends Table
{
	protected $table = 'user';

	public function up()
	{
		$this->scheme()->create($this->table, function (Blueprint $table) {
			$table->increments('id');
			$table->string('username', 30)->nullable(false)->default('');
			$table->integer('join_time')->default(0);
			$table->timestamps();
		});
	}
}