<?php


namespace Tests\Database\Migration\Order;


use Illuminate\Database\Schema\Blueprint;
use Tests\Database\Migration\Table;

class ProductTable extends Table
{
	protected $table = 'product';

	public function up()
	{
		$this->scheme()->create($this->table, function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->index();
			$table->string('title', 50)->default('');
			$table->timestamps();
		});
	}
}