<?php

namespace Tests\Database\Migration\Order;


use Illuminate\Database\Schema\Blueprint;
use Tests\Database\Migration\Table;

class OrderTable extends Table
{
	protected $table = 'order';

	public function up()
	{
		$this->scheme()->create($this->table, function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->default(0)->index();
			$table->integer('amount')->default(0);
			$table->string('state', 10)->default('');
			$table->timestamps();
		});
	}
}