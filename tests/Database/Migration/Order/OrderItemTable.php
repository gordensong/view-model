<?php

namespace Tests\Database\Migration\Order;

use Illuminate\Database\Schema\Blueprint;
use Tests\Database\Migration\Table;

class OrderItemTable extends Table
{
	protected $table = 'order_item';

	public function up()
	{
		$this->scheme()->create($this->table, function (Blueprint $table) {
			$table->increments('id');
			$table->integer('order_id')->default(0)->index();
			$table->integer('product_id')->default(0)->index();
			$table->string('title', 100)->default('');
			$table->integer('amount')->default(0);
		});
	}
}