<?php

namespace Tests\Database\Seeder\Order;

use Illuminate\Support\Facades\DB;
use Tests\Database\Seeder\Seeder;

class UserSeeder extends Seeder
{
	public function run()
	{
		DB::connection('default')
			->table('user')
			->insert([
				['id' => 1, 'username' => 'admin', 'join_time' => time() - 86400 * 2],
				['id' => 2, 'username' => 'buyer', 'join_time' => time() - 86400 * 1],
				['id' => 3, 'username' => 'seller', 'join_time' => time()],
			]);
	}
}