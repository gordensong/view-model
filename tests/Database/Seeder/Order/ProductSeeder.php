<?php


namespace Tests\Database\Seeder\Order;


use Illuminate\Support\Facades\DB;
use Tests\Database\Seeder\Seeder;

class ProductSeeder extends Seeder
{
	public function run()
	{
		DB::table('product')->insert([
			[
				'id' => 1,
				'user_id' => 2,
				'title' => 'product 1',
			],
			[
				'id' => 2,
				'user_id' => 2,
				'title' => 'product 2',
			]
		]);
	}
}