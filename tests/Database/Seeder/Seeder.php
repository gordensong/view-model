<?php


namespace Tests\Database\Seeder;


abstract class Seeder extends \Illuminate\Database\Seeder
{
	abstract public function run();
}

