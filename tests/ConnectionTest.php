<?php

/**
 * WeEngine Sdk Core System
 *
 * (c) We7Team 2019 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace Tests;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ConnectionTest extends TestCase
{
	/**
	 * 测试链接
	 */
	public function testConnection()
	{
		$table = 'demo';

		Schema::dropIfExists($table);
		Schema::connection('default')->create($table, function (Blueprint $table) {
			$table->integer('id')->autoIncrement();
		});

		self::assertTrue(Schema::hasTable($table));
	}
}

