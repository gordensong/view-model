<?php


namespace Tests\Model\Order;


use Tests\Model\Model;

class Product extends Model
{
	protected $table = 'product';

	public $timestamps = true;
}