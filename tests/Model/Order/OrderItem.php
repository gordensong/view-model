<?php


namespace Tests\Model\Order;


use Tests\Model\Model;

/**
 * Class OrderItem
 * @package Tests\Models\Orders
 *
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property int $amount
 * @property string $title
 *
 * @property Product $product
 */
class OrderItem extends Model
{
	public $timestamps = false;

	protected $table = 'order_item';

	protected $guarded = [];

	public function order()
	{
		return $this->belongsTo(Order::class);
	}

	public function product()
	{
		return $this->belongsTo(Product::class);
	}
}