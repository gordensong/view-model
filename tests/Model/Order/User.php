<?php


namespace Tests\Model\Order;


use Tests\Model\Model;

/**
 * Class User
 * @package Tests\Model\Orders
 *
 * @property int $id
 * @property string $username
 */
class User extends Model
{
	protected $table = 'user';
	protected $guarded = [];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function orders()
	{
		return $this->hasMany(Order::class, 'user_id', 'id');
	}
}