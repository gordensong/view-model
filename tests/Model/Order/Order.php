<?php


namespace Tests\Model\Order;


use Illuminate\Database\Eloquent\Collection;
use Tests\Model\Model;

/**
 * Class Order
 * @package Tests\Models\Orders
 *
 * @property int $id
 * @property int $user_id
 * @property int $amount
 * @property int $state
 *
 * @property User $user
 * @property Collection|OrderItem[] $items
 */
class Order extends Model
{
	protected $table = 'order';
	protected $guarded = [];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function items()
	{
		return $this->hasMany(OrderItem::class);
	}
}