<?php

namespace Tests\Model;

abstract class Model extends \Illuminate\Database\Eloquent\Model
{
	/**
	 * @var null
	 */
	public static $connectionName = null;

	/**
	 * @return string|null
	 */
	public function getConnectionName()
	{
		return is_null(static::$connectionName) ? parent::getConnectionName() : static::$connectionName;
	}

	/**
	 * @param $name
	 */
	public static function setDefaultConnection($name)
	{
		static::$connectionName = $name;
	}
}
